# tit
tiny issue tracker - the issue tracker for you own issues not your teams.

## Installation
```sh
git clone https://codeberg.org/benwernicke/tit
cd tit
make release
sudo make install
```
Or specify which editor you want to use and the location and name of your issue
file. Defaults are neovim and `$HOME/tit-issues.md`

### Requirements
- curses
- [belt](https://codeberg.org/benwernicke/belt)
- gcc (maybe other compilers work too, only tested on gcc-13.2)

## General Usage
### Adding an Issue
```sh
tit add "<This is the title of a new Issue>"
```
If you want more information in the issue itself you can pass the `-e/--edit`
flag, which will open up the specified Editor with the current Issue loaded.
```sh
tit add "Issue with Description" --edit
```

### Editing an Issue
```sh
tit edit
```
This will open up a simple `curses` based menu where you can select the wanted
issue. If you have a lot on your plate it might be combersome to mannually
navigate to the relevant issues. In this case you can pass the `-f/--filter`
Flag which will sort the issues based on how much it matches the specified
filter.
```sh
tit edit --filter="coding"
tit edit -f coding
```

### Listing all Issues
```sh
tit ls
tit list
```
This will print the titles of all current issues to stdout. If a specific order
is required you can pass the `-f/--filter` flag.
```sh
tit ls -f uni
```

### Viewing an Issue
```sh
tit view 
```
This will open up the same menu as in `tit edit` and the selected issue will be
printed to stdout. You can also pass the `-f/--filter` flag if a specific
ordering is needed.

### Removing an Issue
```sh
tit remove
tit rm
```
This will open up to same menu as in `tit edit` and the selected issue will be
removed. You can also pass the `-f/--filter` flag to order the issues.

## General Information
`tit` stores all issue in a single markdown file specified by `ISSUE_FILE` in
`Makefile`.

## Goals for the Future
- better menu
- asking if you really want to delete the issue
- better visuals
- making tit more robust against badly input
