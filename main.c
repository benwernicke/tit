#include <belt/flag.h>
#include <belt/match.h>
#include <belt/panic.h>
#include <belt/slice.h>
#include <belt/todo.h>
#include <ctype.h>
#include <curses.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/* ------------------------Entry----------------------------------- */

typedef struct Entry Entry;
struct Entry {
    Str title;
    Str body;
    u32 rank;
};

static bool str_is_valid(Str s) {
    return s.len && s.buf;
}

#define entry_print_body(STREAM, ENTRY) \
    do { \
        Entry* entry__ = ENTRY; \
        FILE* stream__ = STREAM; \
        if (str_is_valid(entry__->body)) { \
            fprintf(stream__, "%.*s", (int)entry__->body.len, entry__->body.buf); \
        } \
    } while (0)

#define entry_print_title(STREAM, ENTRY) \
    do { \
        Entry* entry__ = ENTRY; \
        FILE* stream__ = STREAM; \
        fprintf(stream__, "%.*s\n", (int)entry__->title.len, entry__->title.buf); \
    } while (0)

#define entry_print(STREAM, ENTRY) \
    do { \
        Entry* entry_ = ENTRY; \
        FILE* stream_ = STREAM; \
        entry_print_title(stream_, entry_); \
        entry_print_body(stream_, entry_); \
    } while (0)

/* ------------------------Issue File------------------------------ */

#ifndef ISSUE_FILE
#error "ISSUE_FILE must be defined"
#endif

#ifndef EDITOR
#error "EDITOR must be defined"
#endif /* EDITOR */

#define EDITOR_ EDITOR "    "

MAKE_SLICE(Entry_Slice, Entry)
MAKE_SLICE(File_Slice, char)

static Entry_Slice entries = { };
static File_Slice  file    = { };

static void file_load(void) {
    FILE* f = fopen(ISSUE_FILE, "r");
    PANIC_IF(!f, "could not open file '%s': %s\n", ISSUE_FILE, strerror(errno));

    fseek(f, 0, SEEK_END);
    u32 len = ftell(f);
    rewind(f);

    slice_reserve(&file, len + 512);

    u32 read = fread(file.buf, 1, len, f);
    PANIC_IF(read != len, 
            "could not read file '%s': %s", ISSUE_FILE, strerror(errno));
    file.len = len;
    fclose(f);

    u32 i = 0;

    /* skip space */
    for (; i != file.len && isspace(file.buf[i]); ++i) { }

    while (i != file.len) {
        /* allocate new entry */
        Entry* entry = slice_more(&entries);
        *entry = (Entry) { };

        PANIC_IF(file.buf[i] != '#', "'%s' has wrong format: expect issue title starting with: '#'", ISSUE_FILE);

        /* parse entry title */
        entry->title.buf = &file.buf[i];
        for (; i != file.len && file.buf[i] != '\n'; ++i, ++entry->title.len) { }

        /* skip space */
        for (; isspace(file.buf[i]); ++i) { }

        if (file.buf[i] == '#') continue;

        entry->body.buf = &file.buf[i];
        for (; i != file.len && file.buf[i] != '#'; ++i, ++entry->body.len) { }
    }
}


static void file_unload(void) {
    FILE* f = fopen(ISSUE_FILE, "w");
    PANIC_IF(!f, "could not open file '%s': %s\n", ISSUE_FILE, strerror(errno));

    for (u32 i = 0; i < entries.len; ++i) {
        entry_print(f, &entries.buf[i]);
    }
    free(entries.buf);
    free(file.buf);
    fclose(f);
}

/* ------------------------Flag Globals---------------------------- */

static bool help_flag   = false;
static bool edit_flag   = false;
static Str  filter_flag = { };
static bool view_flag   = false;

#define MODE_NONE   0
#define MODE_EDIT   1
#define MODE_VIEW   2
#define MODE_ADD    3
#define MODE_LIST   4
#define MODE_REMOVE 5

static u32 mode = MODE_NONE;

static Flag flags[] = {
    MAKE_MODE(&mode, MODE_EDIT,   "edit",   "edit selected issue"),
    MAKE_MODE(&mode, MODE_VIEW,   "view",   "view selected issue"),
    MAKE_MODE(&mode, MODE_ADD,    "add",    "add new issue"),
    MAKE_MODE(&mode, MODE_LIST,   "list",   "list all issues"),
    MAKE_MODE(&mode, MODE_LIST,   "ls",     "list all issues"),
    MAKE_MODE(&mode, MODE_REMOVE, "remove", "remove selected issue"),
    MAKE_MODE(&mode, MODE_REMOVE, "rm",     "remove selected issue"),

    MAKE_FLAG(&help_flag,   "-h", "--help",   "print this page and exit"),
    MAKE_FLAG(&edit_flag,   "-e", "--edit",   "edit selected issue"),
    MAKE_FLAG(&filter_flag, "-f", "--filter", "filter elements"),
    MAKE_FLAG(&view_flag,   NULL, "--view",   "view selected issue"),
};
constexpr u32 flags_len = sizeof(flags) / sizeof(*flags);

static constexpr char general_usage[] =   
    "tit - tiny issue tracker\n"
    "~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";

/* ------------------------add------------------------------------- */
static Entry* entry_add(const Str title) {

    /* allocate new entry */
    Entry* entry = slice_more(&entries);
    *entry = (Entry) { };

    slice_reserve(&file, file.len + title.len);
    memcpy(&file.buf[file.len], title.buf, title.len);

    entry->title.buf = &file.buf[file.len];
    file.len += title.len;
    entry->title.len = title.len;

    return entry;
}

/* ------------------------filter---------------------------------- */
int filter_cmp(const void* a, const void* b) {
    return (isize)((Entry*)a)->rank - (isize)((Entry*)b)->rank;
}

static void entry_filter(void) {
    Str filter = filter_flag;

    /* todo how evil is this VLA */
    /* add globbing to filter */
    char filter_[filter.len + 2];
    filter.buf = filter_;

    filter.buf[0] = '*';
    filter.buf[filter.len + 1] = '*';
    memcpy(filter.buf + 1, filter_flag.buf, filter.len);
    filter.len += 2;


    for (u32 i = 0; i < entries.len; ++i) {
        entries.buf[i].rank = fuzzy_glob_score(filter, entries.buf[i].title);
    }
    qsort(entries.buf, entries.len, sizeof(Entry), filter_cmp);
}

/* ------------------------remove---------------------------------- */

static void entry_remove(Entry* entry) {
    slice_remove(&entries, entry - entries.buf);
}

/* ------------------------select---------------------------------- */
static Entry* entry_selector(void) {
    initscr();      /* initialize the curses library */
    keypad(stdscr, TRUE);  /* enable keyboard mapping */
    cbreak();       /* take input chars one at a time, no wait for \n */
    noecho();         /* echo input - in color */
    nl();

    u32 index = 0;

    bool running = 1;
    while (running) {
        for (u32 i = 0; i < entries.len; ++i) {
            move(i + 3, 5);
            if (i == index) {
                addstr("\t> # ");
            } else {
                addstr("\t  # ");
            }
            addnstr(entries.buf[i].title.buf, entries.buf[i].title.len);
        }
        int c = getch();
        switch (c) {
            case KEY_ENTER:
            case '\n':
                running = 0;
                break;
            case 'q':
                endwin();
                exit(0);
                break;
            case 'j':
                index += index < (entries.len - 1);
                break;
            case 'k':
                index -= index > 0;
                break;
            default:
                break;
        }
    }
    endwin();
    return &entries.buf[index];
}

/* ------------------------list------------------------------------ */


static void entry_list(void) {
    for (u32 i = 0; i < entries.len; ++i) {
        entry_print_title(stdout, &entries.buf[i]);
    }
}

/* ------------------------edit------------------------------------ */

/* somehow this definition does not exist but function exists */
int mkstemp(char*);
int dprintf(int fd, const char *restrict format, ...);

static void entry_edit(Entry* entry) {
    char filename[] = "/tmp/tit-tmpfile-XXXXXX";
    int fd = mkstemp(filename);
    PANIC_IF(fd == -1, "could not create temporary file: %s", strerror(errno));

    dprintf(fd, "# %.*s\n", entry->title.len, entry->title.buf);
    if (entry->body.buf) {
        dprintf(fd, "%.*s", entry->body.len, entry->body.buf);
    }

    close(fd);

    static char cmd[sizeof(filename) + sizeof(EDITOR_)] = { };
    memcpy(cmd, EDITOR_, sizeof(EDITOR_));
    memcpy(cmd + (sizeof(EDITOR_) - 1), filename, sizeof(filename) - 1);

    system(cmd);

    FILE* f = fopen(filename, "r");
    PANIC_IF(!f, "could not reread temporary file: %s", strerror(errno));

    fseek(f, 0, SEEK_END);
    u32 len = ftell(f);
    rewind(f);

    slice_reserve(&file, file.len + len);
    u32 read = fread(file.buf + file.len, 1, len, f);
    PANIC_IF(read != len, "could not read tmp file: %s", strerror(errno));

    u32 i = file.len;
    file.len += len;

    /* skip space */
    for (; i != file.len && isspace(file.buf[i]); ++i) { }

    PANIC_IF(file.buf[i] != '#', "'%s' has wrong format: expect issue title starting with: '#'", ISSUE_FILE);
    i += 1;

    /* skip space */
    for (; i != file.len && isspace(file.buf[i]); ++i) { }

    /* reset entry */
    *entry = (Entry) { };

    /* parse entry title */
    entry->title.buf = &file.buf[i];
    for (; i != file.len && file.buf[i] != '\n'; ++i, ++entry->title.len) { }

    /* skip space */
    for (; isspace(file.buf[i]); ++i) { }

    if (i != file.len) {
        entry->body.buf = &file.buf[i];
        for (; i != file.len && file.buf[i] != '#'; ++i, ++entry->body.len) { }
    }

    fclose(f);
    unlink(filename);
}

/* ------------------------main------------------------------------ */

int main(int argc, char* argv[argc]) {

    /* Flag stuff */
    {
        Flag_Error err = flag_parse(
                argc, argv, 
                flags_len, flags,
                .filtered = { &argc, &argv });
        PANIC_IF(err, "could not parse cli flags: %s at '%s'", 
                flag_error_format(err),
                argv[flag_error_at(err)]);
        if (help_flag) {
            flag_print_help(stdout, general_usage, flags_len, flags);
            exit(0);
        }
    }

    file_load();

    Entry* entry = NULL;

    if (str_is_valid(filter_flag)) {
        entry_filter();
    }

    switch (mode) {
        case MODE_NONE:
            fprintf(stderr, "no mode selected - view '%s --help' for further guidance\n", argv[0]);
            exit(1);
            break;

        case MODE_EDIT:
            entry = entry_selector();
            entry_edit(entry);
            break;

        case MODE_VIEW:
            entry = entry_selector();
            entry_print(stdout, entry);
            break;

        case MODE_ADD:
            PANIC_IF(argc != 2, "wrong number of arguments: should be '%s add <title> [flags]'\n", argv[0]);
            entry = entry_add( (Str) { .len = strlen(argv[1]), .buf = argv[1] } );
            break;

        case MODE_LIST:
            entry_list();
            break;

        case MODE_REMOVE:
            entry_remove(entry_selector());
            break;
            
        default:
            fprintf(stderr, "this should not be possible: memory corruption\n");
            __builtin_trap();
    }

    if (edit_flag) {
        PANIC_IF(!entry, "cannot use '--edit' without selective mode");
        entry_edit(entry);
    }

    if (view_flag) {
        PANIC_IF(!entry, "cannot use '--view' without selective mode");
        entry_print(stdout, entry);
    }

    file_unload();
    return 0;
}
