ISSUE_FILE = "${HOME}/tit-issues.md"
EDITOR = "nvim"

FLAGS := -std=c2x -lcurses

CFLAGS := -g -Wall -Wextra -fsanitize=address,leak,undefined ${FLAGS}

all: main.c
	gcc main.c ${CFLAGS} -D'ISSUE_FILE="issue.md"' -D'EDITOR=${EDITOR}'

release: main.c
	gcc main.c ${FLAGS} -march=native -mtune=native -O2 -D'ISSUE_FILE=${ISSUE_FILE}' -D'EDITOR=${EDITOR}' -o tit

perf: main.c
	gcc main.c ${FLAGS} -pg -march=native -mtune=native -O2 -fno-omit-frame-pointer -D'ISSUE_FILE=${ISSUE_FILE}' -D'EDITOR=${EDITOR}'

install:
	cp tit /usr/bin

clean:
	rm -f *.out vec.h tit
